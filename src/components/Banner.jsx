import {Container} from "react-bootstrap"

const Banner = () => {
    return (
        <div>
            <br /><br /><br/>
            <Container className="p-7 mt-5">
                <div className="banner">
                    <h3 align="center">Sewa Mobil Sekarang</h3>
                    <p align="center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <div align="center">
                        <a href="sewa.html" className="btn btn-color-theme pl-3 pr-3 mt-3">Mulai Sewa Mobil</a>
                    </div>
                </div>
            </Container>
            <br /><br />
        </div>
    )
}

export default Banner