import { Container, Carousel, Card, Row, Col, Button} from "react-bootstrap"
import picture from "./assets/image/img_photo.png"
import rate from "./assets/image/Rate.png"

const Testimonial = () => {
    return (
        <div>
            <br/><br/>
            <div className="testimonial p-7 mt-5">
                <Container>
                    <h3 align="center" className="mb-3">Testimonial</h3>
                    <p align="center" className="mb-3">Berbagai review positif dari para pelanggan kami</p>
                    <Carousel id="carouselExampleControls" className="carousel slide mt-1" data-bs-ride="carousel">
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <Card className="card bg-testi py-5 px-3">
                                    <Row>
                                        <Col className="col-lg-2">
                                            <img src={picture} alt="" />
                                        </Col>
                                        <Col className="col-lg-10">
                                            <div className="d-flex">
                                               <img src={rate} alt="" /> 
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                            <p class="fw-bold">John Dee 32, Bromo</p>
                                        </Col>
                                    </Row>
                                </Card>
                            </div>
                            <div className="d-flex justify-content-center gap-2 mt-3">
                                <Button className="carousel-control carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Previous</span>
                                </Button>
                                <Button className="carousel-control carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Next</span>
                                </Button>
                            </div>
                        </div>
                    </Carousel>
                </Container>
            </div>
        </div>
    )
}

export default Testimonial