import {Container, Col, Row} from "react-bootstrap"
import logo from "./assets/image/logo.png"
import facebook from "./assets/image/icon_facebook.png"
import instagram from "./assets/image/icon_instagram.png"
import twitter from "./assets/image/icon_twitter.png"
import email from "./assets/image/icon_mail.png"
import twitch from "./assets/image/icon_twitch.png"

const Footer = () => {
    return (
        // <!-- Footer Start -->
    <div className="container-fluid footer mt-5 pt-5 wow fadeIn" data-wow-delay="0.1s">
        <Container className="container py-5">
            <Row className="row g-5">
                <Col className="col-lg-3 col-md-6">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </Col>
                <Col className="col-lg-3 col-md-6">
                    <p className="btn btn-link" href="">Our Service</p>
                    <p className="btn btn-link" href="">Why Us</p>
                    <p className="btn btn-link" href="">Testimonial</p>
                    <p className="btn btn-link" href="">FAQ</p>
                </Col>
                <Col className="col-lg-3 col-md-6">
                    <h6 className="mb-3">Connect With Us</h6>
                    <div className="d-flex">
                        <img src={facebook} alt=""/>
                        <img src={instagram} alt="" className="ms-1"/>
                        <img src={twitter} alt="" className="ms-1"/>
                        <img src={email} alt="" className="ms-1"/>
                        <img src={twitch} alt="" className="ms-1"/>
                    </div>
                </Col>
                <Col className="col-lg-3 col-md-6">
                    <h6 className="mb-3">Copyright Binar 2022</h6>
                    <img src={logo} alt="" />
                </Col>
            </Row>
        </Container>
    </div>
    // <!-- Footer End -->
    )
}

export default Footer