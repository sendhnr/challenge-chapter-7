import {Container, Row, Col, Card} from "react-bootstrap"
import thumbsup from "./assets/image/icon_complete.png"
import price from "./assets/image/icon_price.png"
import clock from "./assets/image/icon_24hrs.png"
import profesional from "./assets/image/icon_professional.png"

const WhyUsSection = () => {
    return (
        <div>
            <br/><br/>
            <Container>
                <section id="whyus">
                    <div className="text-black mt-5">
                        <h2 className="fw-bold mb-3">Why Us</h2>
                        <p>Mengapa harus pilih Binar Car Rental</p>
                    </div>
                    <Row className="row cols-1 row-cols-md-2 row-cols-lg-4 g-4 text-black" >
                        <Col>
                            <Card className="card border-secondary mb-3">
                                <Card.Body>
                                    <img src={thumbsup} alt="" />
                                    <h3 class="card-title mb-3 fs-5 fw-bold">Mobil Lengkap</h3>
                                    <p class="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card className="card border-secondary mb-3">
                                <Card.Body>
                                <img src={price} alt="" />
                                    <h3 class="card-title mb-3 fs-5 fw-bold">Harga Murah</h3>
                                    <p class="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card className="card border-secondary mb-3">
                                <Card.Body>
                                <img src={clock} alt="" />
                                    <h3 class="card-title mb-3 fs-5 fw-bold">Layanan 24 Jam</h3>
                                    <p class="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card className="card border-secondary mb-3">
                                <Card.Body>
                                <img src={profesional} alt="" />
                                    <h3 class="card-title mb-3 fs-5 fw-bold">Sopir Profesional</h3>
                                    <p class="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </section>
            </Container>
        </div>
    )
}

export default WhyUsSection