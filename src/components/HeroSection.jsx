import {Row, Col} from "react-bootstrap"
import imageCar from "./assets/image/img_car.png"
import "./assets/css/LandingPage.css"

const HeroSection = () => {
    return (
        <div>
            <br /><br /><br/>
            <div className="herosection">
                <div className="mt-5">
                    <Row>
                        <Col className="col-lg-6">
                            <div className="caption">
                                <h2><b>Sewa & Rental Mobil Terbaik di kawasan Cimahi</b></h2>
                                <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                                <a className="btn btn-color-theme pl-3 pr-3" href="RentCar.jsx">Mulai Sewa Mobil</a>
                            </div>
                        </Col>
                        <Col className="col-lg-6">
                            <div className="imageCar">
                                <img src={imageCar} alt="" />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>
    )
}

export default HeroSection