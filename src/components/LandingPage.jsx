import HeroSection from "./HeroSection";
import NavigationBar from "./NavigationBar";
import "./assets/css/LandingPage.css"
import OurServiceSection from "./OurServiceSection";
import WhyUsSection from "./WhyUsSection";
import Testimonial from "./Testimonial";
import Footer from "./Footer";
import Banner from "./Banner";
import FaqSection from "./FaqSection";

const LandingPage = () => {
    return (
        <div>
            <div className="header">
                <NavigationBar/>
                <HeroSection/>
            </div>
            <div>
                <OurServiceSection/>
            </div>
            <div>
                <WhyUsSection/>
            </div>
            <div>
                <Testimonial/>
            </div>
            <div>
                <Banner/>
            </div>
            <div>
                <FaqSection/>
            </div>
            <div>
                <Footer/>
            </div>
        </div>
    )
}

export default LandingPage