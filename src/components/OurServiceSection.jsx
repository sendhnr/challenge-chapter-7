import "./assets/css/LandingPage.css"
import {Container, Row, Col} from "react-bootstrap"
import girl from "./assets/image/img_service.png"
import checklist from "./assets/image/checklist.png"

const OurServiceSection = () => {
    return (
        <div>
            <br/><br/>
            <Container>
                <Row id="ourservices">
                    <Col className="col-lg-6">
                        <Col className="col-lg-12 mt-4">
                            <img src={girl} alt="" />
                        </Col>
                    </Col>
                    <Col className="col-lg-6 mt-5">
                        <div className="services">
                            <div className="mt-4">
                                <div className="services-text">
                                    <h4><b>Best Car Rental for any kind of trip in Cimahi!</b></h4>
                                    <p>Sewa mobil bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                                    <img src={checklist} alt=""/>       Sewa Mobil Dengan Supir 12 Jam
                                    <br/><br/>
                                    <img src={checklist} alt=""/>       Sewa Mobil Lepas Kunci 24 Jam
                                    <br/><br/>
                                    <img src={checklist} alt=""/>       Sewa Mobil Jangka Panjang Bulanan
                                    <br/><br/>
                                    <img src={checklist} alt=""/>       Gratis Antar - Jemput Mobil di Bandara
                                    <br/><br/>
                                    <img src={checklist} alt=""/>       Layanan Airport Transfer / Drop In Out
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default OurServiceSection