import {Nav, Container, Navbar} from "react-bootstrap"
import logo from "../components/assets/image/logo.png"

const NavigationBar = () => {
    return (
        <div>
            <Navbar>
                <Container>
                    <Navbar.Brand><img src={logo} alt="" /></Navbar.Brand>
                    <Nav>
                        <Nav.Link>Our Service</Nav.Link>
                        <Nav.Link>Why Us</Nav.Link>
                        <Nav.Link>Testimonial</Nav.Link>
                        <Nav.Link>FAQ</Nav.Link>
                        <Nav.Link className="btn btn-color-theme pl-3 pr-3 ms-2">Register</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </div>
    )
}

export default NavigationBar