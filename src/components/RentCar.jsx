import HeroSection from "./HeroSection"
import NavigationBar from "./NavigationBar"
import Footer from "./Footer"
import "./assets/css/LandingPage.css"
import SliceCard from "./Card"

const RentCar = () => {
    return (
        <div>
            <div className="header">
                <NavigationBar/>
                <HeroSection/>
            </div>
            <div>
                <SliceCard/>
            </div>
            <div>
                <Footer/>
            </div>
        </div>
    )
}

export default RentCar